#!/usr/bin/env python3
# encoding: utf-8

"""
This script permits to divide up a file into f fragments using Shamir Sharing
Secret algorithm and to combine k fragments back into a file.

TODO: Add verbose while dividing and combining
TODO: Task parallelization
TODO: Write shared files together
TODO: Minimize cpu time
"""

import random
import argparse

# Maximal number of fragments
max_fragments = 256
# Prime number, smallest as possible and superior to max_fragments
prime = 257
# Invert modulo of each number between 1 and 256
invert = [0,
    1,129,86,193,103,43,147,225,200,180,187,150,178,202,120,241,121,100,230,90,
    49,222,190,75,72,89,238,101,195,60,199,249,148,189,235,50,132,115,145,45,
    163,153,6,111,40,95,175,166,21,36,126,173,97,119,243,179,248,226,61,30,59,
    228,102,253,87,74,234,223,149,246,181,25,169,66,24,186,247,201,244,151,165,
    210,96,205,127,3,65,184,26,20,209,176,152,216,46,83,53,139,135,18,28,63,5,
    215,164,177,245,188,224,250,44,218,116,124,38,113,134,159,54,15,17,158,140,
    114,220,51,85,255,2,172,206,37,143,117,99,240,242,203,98,123,144,219,133,
    141,39,213,7,33,69,12,80,93,42,252,194,229,239,122,118,204,174,211,41,105,
    81,48,237,231,73,192,254,130,52,161,47,92,106,13,56,10,71,233,191,88,232,
    76,11,108,34,23,183,170,4,155,29,198,227,196,31,9,78,14,138,160,84,131,221,
    236,91,82,162,217,146,251,104,94,212,112,142,125,207,22,68,109,8,58,197,62,
    156,19,168,185,182,67,35,208,167,27,157,136,16,137,55,79,107,70,77,57,32,
    110,214,154,64,171,128,256,
]

def parse_number(number):
    """
    This function validates the k argument.
    """
    msg = "k argument should be an integer between 2 and 256, not {0}".format(number)
    try:
        value = int(number)
    except:
        raise argparse.ArgumentTypeError(msg)
    if value<2 or value>256:
        raise argparse.ArgumentTypeError(msg)
    return value

def parse_args():
    """
    This function parses arguments. There are two commands and each one has
    its own options.
    """
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(title="commands", description="List of commands:")
    # subparser for command 'divide'
    command_divide = subparsers.add_parser(
        "divide",
        help="Divide up a secret in fragments",
    )
    command_divide.add_argument(
        "secret",
        help="filename containing the secret to share",
    )
    command_divide.add_argument(
        "--slices",
        type=str,
        help="string indicating which fragments will be generated (Default: all)",
    )
    command_divide.add_argument(
        "--k",
        type=parse_number,
        default=3,
        metavar="NUMBER",
        help="minimal number of fragments needed in order to reconstruct the secret (Default: 3, Min: 2, Max: 256)",
    )
    command_divide.add_argument(
        "--seed",
        help="seed used to generate random coefficients. Usefull when generating new fragments of a previously shared secret"
    )
    command_divide.set_defaults(command="divide")
    # subparser for command 'combine'
    command_combine = subparsers.add_parser(
        "combine",
        help="Combine some fragments in order to rebuild a secret",
    )
    command_combine.add_argument(
        "fragments",
        nargs="+",
        help="list of files in order to reconstruct the secret",
    )
    command_combine.set_defaults(command="combine")
    return parser.parse_args()

def bytes_from_file(filename, chunksize=8192):
    """
    This function reads from a file byte per byte, from a chunksize buffer.

    Great thanks to codeape (http://stackoverflow.com/users/3571/codeape)
    and stackoverflow (http://stackoverflow.com/a/1035456)
    """
    with open(filename, "rb") as f:
        while True:
            chunk = f.read(chunksize)
            if chunk:
                for b in chunk:
                    yield b
            else:
                break

def invert_modulo(number):
    """
    This function calculates the value of an inverted number modulo prime. Since
    the inverted number has the same modulo than its modulo, we can simplify the
    calculation by using a simple array.
    """
    new_number = number % prime
    return invert[new_number]
    #msg = "Gasp! It seems that {0} could not modulo-inverted with {1} !".format(number, prime)
    #for coefficient in range(0, prime):
    #    if (((coefficient*number)%prime)==1):
    #        return coefficient
    #raise Exception(msg)

def divide(secret, number, seed, slices):
    """
    Divide the secret file into number files using Shamir Sharing Secret
    algorithm
    """
    if slices is None:
        slices = range(1,max_fragments)
    else:
        try:
            slices = eval(slices)
        except:
            raise TypeError("slices must be a tuple of integers or a range")
        if not isinstance(slices,(tuple,range)):
            print(type(slices))
            raise TypeError("slices must be a tuple of integers or a range")
        else:
            for slice in slices:
                if not isinstance(slice,int):
                    raise TypeError("slices must be a tuple of integers or a range")

    # Calculation of coefficients
    if seed is not None:
        random.seed(hash(seed))
    coefficients = [0,]
    for index in range(1,number):
        coefficients.append(random.randint(1,max_fragments))
    coefficients.reverse()

    for slice in slices:
        print("Calculating fragment {0}".format(slice))
        # Buffer initialized with first byte being the identifier of the fragment
        buffer = bytearray()
        buffer.append(slice)

        # Dividing the secret in fragment
        for byte in bytes_from_file(secret):
            coefficients[-1] = byte
            result = 0
            for coeff in coefficients:
                result = result * slice + coeff
            result %= prime
            # We need to store only bytes
            while result>=255:
                result -= 255
                buffer.append(255)
            buffer.append(result)

        # Writing fragment
        with open("fragment_"+str(slice), "wb") as f:
            f.write(buffer)
        print("Done !")

def combine(fragments):
    """
    Combine some fragments into a file using Shamir Sharing Secret algorithm
    """
    number = len(fragments)

    # Calculating Lagrange coefficients based on fragments identifiers
    identifiers = []
    for fragment in fragments:
        with open(fragment, "rb") as f:
            identifiers.append(int.from_bytes(f.read(1), byteorder='big'))

    numerators = {}
    denominators = {}
    for index in range(0,len(fragments)):
        numerators[index] = 1
        denominators[index] = 1
        for index_2 in range(0,len(fragments)):
            if index_2!=index:
                numerators[index] *= -identifiers[index_2]
                denominators[index] *= identifiers[index]-identifiers[index_2]
        numerators[index] %= prime
        denominators[index] = invert_modulo(denominators[index])

    # Rebuilding secret by combining fragments
    secret = {}
    for index in range(0,len(fragments)):
        offset = 0
        counter = 0
        overflow = False
        future_byte = 0
        for byte in bytes_from_file(fragments[index]):
            # First byte is useless
            if counter>0:
                # If byte is 255, we have to sum it with the next one(s)
                if byte==255:
                    overflow = True
                    future_byte += 255
                else:
                    if overflow:
                        overflow = False
                    future_byte += byte
                    if offset in secret:
                        secret[offset] += (future_byte*numerators[index]*denominators[index])%prime
                    else:
                        secret[offset] = (future_byte*numerators[index]*denominators[index])%prime
                    future_byte = 0
                    offset += 1
            counter += 1

    # Cleaning the secret
    for byte in secret:
        secret[byte] %= prime

    with open("shared_secret", "wb") as f:
        f.write(bytearray(secret.values()))


if __name__=="__main__":
    args = parse_args()
    if args.command=="divide":
        divide(args.secret, args.k, args.seed, args.slices)
    else:
        combine(args.fragments)
